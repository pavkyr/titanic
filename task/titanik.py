import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    df['Title'] = df['Name'].str.extract(r' ([A-Za-z]+)\.')

    # Define the titles to consider
    titles_to_consider = ['Mr', 'Mrs', 'Miss']

    # Initialize a list to store results
    results = []

    # Iterate over each title and calculate the median age and count of missing values
    for title in titles_to_consider:
        median_age = df.loc[df['Title'] == title, 'Age'].median()
        missing_values = df.loc[(df['Title'] == title) & (df['Age'].isnull())].shape[0]

        # Round the median age to the nearest integer
        median_age = round(median_age)

        # Append the results as a tuple to the list
        results.append((title + '.', missing_values, median_age))

    return results

